#!/bin/bash

export GOPATH="$LANG_PATH/dist"

#*******************************************************************************

export MINIO_ACCESS_KEY=minio
export MINIO_SECRET_KEY=miniostorage

################################################################################

ronin_require_go () {
    for pth in $RONIN_CODE/go/bin ; do
        if [[ ! -d $pth ]] ; then
            mkdir -p $pth
        fi
    done

    cd $RONIN_CODE/go/bin

    if [[ ! -f minio ]] ; then
        target=$HOSTTYPE

        if [[ $target == "i686" ]] ; then
            target="386"
        fi

        wget -c https://dl.minio.io/server/minio/release/linux-$target/minio
    fi

    cd $RONIN_ROOT
}

#*******************************************************************************

ronin_include_go () {
    motd_text "    -> Go      : "$GOPATH
}

